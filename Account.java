public class Account{
	// Variables 
	// -- User Information --
	// Username - String - Unique 
	private int userID;

	private static int nextAccount = 0;
	
	// Password - ?? Conditions
	private String password;
	
	// Name - 
	private String name;
	
	// Age - // should be positive
	private int age;

	// -- Account information -- 
	// balance - double - negative balance means they are in debt 
	private double balance;


	// user methods
	// -- User Information methods -- 
	// Constructors
	// New user - name
	public Account(String newName){
		name = newName;
		age = 0; // default value
		userID = nextAccount;
		nextAccount++; // increment the counter

		// password = ??
		balance = 0; 
		// Get input from user?
	}
	// New user - name, age
	// New user - name, age, balance

	// add money to this account
	// returns the current balance
	// must be positive
	public boolean deposit(double amount){
		if( amount > 0){
			balance = balance + amount;
			return true;
		}else{
			return false;
		}
	} 

	public double returnBalance(){
		return balance;
	}
	// ?? conditions

	// moneyOut(double amount) // remove money from the account
	// ?? conditions

	public String toString(){
		return "account ID: " + userID + " name: " + name + " age:" + age + " balance: " + balance + "\n";
	}

}
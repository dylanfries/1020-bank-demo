public class Bank{

	// list of <Account> [0,100]
	private int maxAccounts = 10;
	private Account[] accounts;

	// max money = 1000.00

	// -- Account information methods -- 
	// Deposit - double - modify the balance by adding the deposit
	// Find a user account and then add the deposit amount to the balance			- 
	// [ ] Verify user
	// ?? Conditions

	// Withdrawl - 
	// find a user account and remove the withdrawl amount. 
	// [ ] Verify user
	// ?? Conditions

	// --- Static Method --- 
	// main method
	// start up a new bank object and 
	public static void main(String[] args){

		Bank b0 = new Bank(7);
		//b0.testData();

		Bank b1 = new Bank(5);

		Bank b2 = new Bank();
		// -- test methods --
		
		// // test data
		// bankDeposit(65.0);
		// bankDeposit(95.0);
		// bankDeposit(0.0);
		// bankDeposit(-999865.0555);
		// bankDeposit(-0.000555);

		System.out.println("Bank 0" + b0.toString());
		System.out.println("Bank 1" + b1.toString());
		System.out.println("Bank 2" + b2.toString());



		// -- output user --
		// get the value from the toString


		// print it to the screen
		// System.out.println(temp);
		// System.out.println(testAccount1.toString());
		// System.out.println(testAccount2.toString());

		// test transactions

		// output

	}

	public Bank(){
		// Create users using the Account constructor. 
 		accounts = new Account[maxAccounts];

 		// Create the actual accounts
 		for(int i = 0; i < accounts.length; i++ ){
 			accounts[i] = new Account("Default");
 		}
		
	}

	public Bank (int numberOfAccount){
		// Create users using the Account constructor. 
 		accounts = new Account[numberOfAccount];

 		// Create the actual accounts
 		for(int i = 0; i < accounts.length; i++ ){
 			accounts[i] = new Account("Default");
 		}

	}

	// private void testData(){


	// 	bankDeposit(65.0);
	// 	bankDeposit(95.0);
	// 	bankDeposit(0.0);
	// 	bankDeposit(-999865.0555);
	// 	bankDeposit(-0.000555);
	// }

	// // [ ] later we'll want to pass a account
	// private void bankDeposit(double amount){
	// 	// Try the account depo or print an error
	// 	if( testAccount.deposit(amount)){
	// 		System.out.println("Deposit Successful: " + amount);
	// 	}else{
	// 		System.out.println(" Transaction Failed: Deposit " + amount);
	// 	}
	// }

	public String toString(){
		String returnString = " Bank : \n";

		for(int i = 0; i < accounts.length; i++){
			returnString += accounts[i].toString();
		}

		return returnString;
	}

}